const db = require('../config/db');

async function salvarUsuario(nome, email, senha) {
    const user =  await db('usuarios')
        .select('id', 'nome', 'email', 'senha').where({ email });
    
    const novoUsuario = { ...user[0], nome, email, senha };
    
    if(user[0]) {
        await db('usuarios')
            .where({ id: user[0].id })
            .update({ ...novoUsuario });
    } else {
        const id = await db('usuarios').insert(novoUsuario);
        novoUsuario.id = id[0];
    }

    return novoUsuario;
}

async function salvarPerfil(nome, rotulo) {
    const perfil = await db('perfis').select('id', 'nome','rotulo').where({ nome });

    const novoPerfil = { ...perfil[0], nome, rotulo };

    if(perfil[0]) {
        await db('perfis')
            .where({ nome: perfil[0].nome })
            .update({ ...novoPerfil });
    } else {
        const id = await db('perfis').insert(novoPerfil);
        novoPerfil.id = id[0];
    }

    return novoPerfil;
}

async function adicionarPerfil(usuario, ...perfis) {
    for(perfil of perfis) {
        await db('usuarios_perfis').insert({ usuario_id: usuario.id, perfil_id: perfil.id });
    }
}

async function executar() {
    const usuario = await salvarUsuario('Ana', 'ana@empresa.com.br', '123456');
    const perfilA = await salvarPerfil('rh', 'Depto. Pessoal');
    const perfilB = await salvarPerfil('fin', 'Financeiro');
    // const perfilC = await salvarPerfil('mkt', 'Marketing');

    console.log(usuario);
    console.log(perfilA);
    console.log(perfilB);
    // console.log(perfilC);

    adicionarPerfil(usuario, perfilA, perfilB);
}


executar()
    .catch(err => console.log(err))
    .finally(() => db.destroy())