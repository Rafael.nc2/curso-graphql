const db = require('../config/db');

// db('perfis')
//     // .then(res => console.table(res))
//     // .then(res => res.map(p => p.nome))
//     .map(p => p.nome)
//     .then(nomes => console.table(nomes))
//     .finally(() => db.destroy());

// db('perfis').select('nome', 'id')
//     .then(res => console.log(res))
//     .finally(() => db.destroy());

// db.select('nome', 'id')
//     .from('perfis')
//     .limit(4) // limita o resulta em 4 linhas
//     .offset(2) // começa a partir do item 2
//     .then(res => console.table(res))
//     .finally(() => db.destroy());

db('perfis')
    .select('id','nome')
    // .where({ id: 2 })
    // .where( 'id', '=', '2')
    // .where('nome', 'like', '%m%')
    // .whereNot({ id: 2 })
    .whereIn('id', [1,2,3])

    //.first() // traz o primeiro elemento encontrado
    .then(res => console.table(res))
    .finally(() => db.destroy())