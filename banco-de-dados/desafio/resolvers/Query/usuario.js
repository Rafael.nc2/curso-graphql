const db = require('../../config/db')

function buscaUsuario({id, email}) {
    if(id) {
        return db('usuarios')
            .where({ id }).first();

    } else if(email) {
        return db('usuarios')
            .where({ email }).first();
    }

    return null;
}

module.exports = {
    usuarios() {
        const usuarios = db('usuarios');

        if(usuarios)
            return usuarios

        return [];
    },

    usuario(_, { filtro }) {
        const usuario = buscaUsuario(filtro);
        return usuario;
    },
}