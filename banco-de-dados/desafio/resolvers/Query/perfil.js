const db = require('../../config/db')

async function buscarPerfil({id, nome}) {
    if(id) {
        return db('perfis').where({ id }).first();
    }else if(nome) {
        return db('perfis').where({ nome }).first();
    }

    return null;
}

module.exports = {
    async perfis() {
        return await db('perfis').select('id','nome','rotulo');
    },
    async perfil(_, { filtro }) {
        // implementar
        const perfil = buscarPerfil(filtro);
        if(perfil)
            return perfil;

        return null;
    }
}