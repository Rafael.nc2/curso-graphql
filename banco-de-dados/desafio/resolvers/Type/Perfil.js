const db = require('../../config/db')

module.exports = {
    usuarios(perfil) {
        return db('usuarios')
            .innerJoin('usuarios_perfis', 'usuarios.id', 'usuarios_perfis.usuario_id')
            .where('usuarios_perfis.perfil_id', '=', perfil.id);
    }
}