const db = require('../../config/db')

module.exports = {
    perfis(usuario) {
        return db('perfis')
            .innerJoin('usuarios_perfis', 'perfis.id', 'usuarios_perfis.perfil_id')
            .where('usuarios_perfis.usuario_id', '=', usuario.id);
    }
}