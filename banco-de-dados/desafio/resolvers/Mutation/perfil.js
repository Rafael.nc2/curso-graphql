const db = require('../../config/db')

async function buscarPerfil({id, nome}) {
    if(id) {
        return db('perfis').where({ id }).first();
    }else if(nome) {
        return db('perfis').where({ nome }).first();
    }

    return null;
}

module.exports = {
    async novoPerfil(_, { dados }) {
        const novoPerfil = dados;
        if(dados) {
            const [ id ] = await db('perfis')
                .insert(novoPerfil);

            novoPerfil.id = id;
        }

        return novoPerfil;
    },
    async excluirPerfil(_, { filtro }) {
        let perfil = await buscarPerfil(filtro);
        if(perfil) {
            await db('perfis')
                .where({ id: perfil.id })
                .delete();

            return perfil;
        }

        return null;
    },
    async alterarPerfil(_, { filtro, dados }) {
        let perfil = await buscarPerfil(filtro);

        if(perfil) {
            perfil = {
                ...perfil,
                ...dados
            };

            await db('perfis')
                .where({ id: perfil.id })
                .update(perfil);

            return perfil;
        }

        return null;
    }
}