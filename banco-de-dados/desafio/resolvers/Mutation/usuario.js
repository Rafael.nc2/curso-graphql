const db = require('../../config/db')

async function buscaUsuario({id, email}) {
    if(id) {
        return await db('usuarios')
            .select('id', 'nome', 'email')
            .where({ id }).first();

    } else if(email) {
        return await db('usuarios')
            .select('id', 'nome', 'email')
            .where({ email }).first();
    }

    return null;
}

module.exports = {
    async novoUsuario(_, { dados }) {
        if(dados) {
            const { perfis } = dados;
            const { nome, email, senha } = dados;
    
            const [ id ] = await db('usuarios')
                .insert({ nome, email, senha });

            if(id) {
                for(perfil of perfis) {
                    await db('usuarios_perfis')
                        .insert({usuario_id: id, perfil_id: perfil.id});

                }
            }

            const novoUsuario = {
                id, 
                nome, 
                email, 
                senha, 
                perfis
            }

            return novoUsuario;
        }

        return null;
    },
    async excluirUsuario(_, { filtro }) {
        const usuario = await buscaUsuario(filtro);
        if(usuario) {
            await db('usuarios_perfis')
                .where( 'usuario_id', '=', usuario.id )
                .delete();

            await db('usuarios')
                .where({ id: usuario.id })
                .delete();

            return usuario;
        }

        return null;;
    },
    async alterarUsuario(_, { filtro, dados }) {
        let usuario = await buscaUsuario(filtro);
        const { nome, email, perfis } = dados;

        if(usuario) {
            await db('usuarios_perfis')
                .where( 'usuario_id', '=', usuario.id )
                .delete();

            for(perfil of perfis) {
                await db('usuarios_perfis')
                    .insert({usuario_id: usuario.id, perfil_id: perfil.id});

            }

            usuario = {
                ...usuario,
                nome,
                email
            }

            await db('usuarios')
                .where({ id: usuario.id })
                .update( usuario );

            return usuario;
        }
    }
}