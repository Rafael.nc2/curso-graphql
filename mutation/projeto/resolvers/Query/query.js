const usuarios = require('./usuario');
const perfis = require('./perfis');

module.exports = {
    ...usuarios,
    ...perfis
}