const { perfis, proximoId } = require('../../data/db');

module.exports = {

    novoPerfil(_, { nome }) {
        const perfilExistente = perfis
            .some(p => p.nome === nome);

        if(perfilExistente){
            throw new Error('Perfil ja cadastrado!');
        }

        const novo = {
            id: proximoId(),
            nome: nome
        }

        perfis.push(novo);
        return novo;
    },

    excluirPerfil(_, { id }) {
        if(id) {
            const i = perfis.findIndex(p => p.id === id);
            if(i < 0) return null;

            const excluido = perfis.splice(i, 1);
            return excluido ? excluido[0] : null;
        }
    },

    alterarPerfil(_, { id, dados }) {
        if(id) {
            const i = perfis.findIndex(p => p.id === id);
            if(i < 0) return null;

            const perfil = {
                id,
                ...dados
            };

            perfis.splice(i,1, perfil);
            return perfil;
        }
    }

}
