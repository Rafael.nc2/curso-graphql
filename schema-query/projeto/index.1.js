const { ApolloServer, gql } = require('apollo-server');

const usuarios = [
    { id: 1, nome: "Natália Dionizio Naville", email: "natydio@gmail.com", idade: 2, perfil_id: 1 },
    { id: 2, nome: "Helena Dionizio Naville", email: "helenadio@gmail.com", idade: 36, perfil_id: 2 },
    { id: 3, nome: "Rafael Naville Contesini", email: "rafael.nc2@gmail.com", idade: 34, perfil_id: 1 }
];

const perfis = [
    { id: 1, nome: 'Comum'},
    { id: 2, nome: 'Administrador'}
];

// tagged template ES6 2015
// É exatamente como serão mapeados os dados dentro da api graphQL
// Para retornar um tipo que não é scalar existente no graphQL, basta declarar
// o tipo desejado dentro do typeDfes. Conforme exemplo do "Date"
const typeDefs = gql`
    # Quando não existe um tipo definido para o graphQl (caso do Date por exemplo)
    # é possível criar e evitar erros de tipo de desconhecido
    scalar Date

    # tipos "scalar" são como se fossem tipos primitivos, 
    # que não tem atributos como objetos
    
    type Produto {
        id: ID
        nome: String!
        preco: Float!
        desconto: Float
        precoComDesconto: Float
    }

    type Perfil {
        id: Int,
        nome: String!
    }

    type Usuario {
        id: Int
        nome: String!
        email: String!
        idade: Int
        salario: Float
        vip: Boolean,
        perfil: Perfil
    }

    # Pontos de entrada da sua API
    # Ao colocar um exclamação depois do tipo "String", o retorno se torna obrigatório ser uma String (Ponto de muita atenção!)
    type Query {
        ola: String!
        horaAtual: Date!
        usuarioLogado: Usuario,
        produtoEmDestaque: Produto,
        numerosMegaSena: [Int!]!,
        usuarios:[Usuario],
        usuario(id: Int): Usuario,
        perfis: [Perfil],
        perfil(id: Int): Perfil
    }
`;

// Conjunto de funções para resolver os dados de formas diferentes
const resolvers = {
    // Resolver para usuário
    Usuario: {
        salario(usuario) {
            // Quando o nome do atributo do objeto não é o mesmo que retornado
            // pelo consulta no banco, é preciso fazer, como se fosse um mapeamento
            // para dizer ao graphQl qual é o campo a ser retornado 
            // quando aquele atributo é chamado
            return usuario.salario_real
        },
        perfil(usuario) {
            const sels = perfis 
                .filter(p => p.id === usuario.perfil_id);
            
            return sels ? sels[0] : null;
        }
    },
    Produto: {
        precoComDesconto(produto) {
            if( produto.desconto ) {
                return produto.preco * (1 - produto.desconto)
            } else {
                return produto.preco
            }
        }
    },
    Query: {
        ola() {
            return 'Bom dia!'
        },

        horaAtual() {
            return new Date
        },

        usuarioLogado() {
            return {
                id: 1,
                nome: "Ana da web",
                email: "anaodaweb@email.com",
                idade: 23,
                salario_real: 1234.56,
                vip: true
            }
        },

        produtoEmDestaque() {
            return {
                id: 1,
                nome: 'Notebook',
                preco: 5000.89,
                desconto: 0.5
            }
        },

        numerosMegaSena() {
            //return [4, 8, 13, 27, 33, 54];
            const crescente = (a, b) => a - b;
            return Array(6).fill(0)
                .map(() => parseInt(Math.random() * 60 + 1))
                .sort(crescente);
        },

        usuarios() {
            return usuarios;
        },
        usuario(_, { id }) {
            // primeiro parâmetro sempre é o proprio objeto pai
            // no caso da query sempre vem como "undefined" em outros resolvers, sim ,é o pai
            // O segundo parâmetro são os argumentos da consulta, neste caso, o ID passado
            const sels = usuarios
                .filter(u => u.id === id)

            return sels ? sels[0] : null;
        },

        perfis() {
            return perfis;
        },
        perfil(_, { id }) {
            const sels = perfis 
                .filter(p => p.id === id)

            return sels ? sels[0] : null;
        }
    }
};

// A configuração do apolloServer requer exatamente estes dois parametros,
// no formato que está escrito. Portanto, se não quiser chamar o primeiro parametro
// de typeDefs, por exemplo, a definição deve ser feita conforme são feitas as atribuições
// para os atributos de um objeto 
// typeDefs: a
const server = new ApolloServer({
    typeDefs,
    resolvers
});

server.listen()
    .then( ({ url }) =>{
        console.log(`Executando em ${url}`);
    })