const { perfis } = require('../data/db');

module.exports = {
    salario(usuario) {
        // Quando o nome do atributo do objeto não é o mesmo que retornado
        // pelo consulta no banco, é preciso fazer, como se fosse um mapeamento
        // para dizer ao graphQl qual é o campo a ser retornado 
        // quando aquele atributo é chamado
        return usuario.salario_real
    },
    perfil(usuario) {
        const sels = perfis 
            .filter(p => p.id === usuario.perfil_id);
        
        return sels ? sels[0] : null;
    }
}
