const { usuarios, perfis } = require('../data/db');

module.exports = {
    ola() {
        return 'Bom dia!'
    },

    horaAtual() {
        return new Date
    },

    usuarioLogado() {
        return {
            id: 1,
            nome: "Ana da web",
            email: "anaodaweb@email.com",
            idade: 23,
            salario_real: 1234.56,
            vip: true
        }
    },

    produtoEmDestaque() {
        return {
            id: 1,
            nome: 'Notebook',
            preco: 5000.89,
            desconto: 0.5
        }
    },

    numerosMegaSena() {
        //return [4, 8, 13, 27, 33, 54];
        const crescente = (a, b) => a - b;
        return Array(6).fill(0)
            .map(() => parseInt(Math.random() * 60 + 1))
            .sort(crescente);
    },

    usuarios() {
        return usuarios;
    },
    usuario(_, { id }) {
        // primeiro parâmetro sempre é o proprio objeto pai
        // no caso da query sempre vem como "undefined" em outros resolvers, sim ,é o pai
        // O segundo parâmetro são os argumentos da consulta, neste caso, o ID passado
        const sels = usuarios
            .filter(u => u.id === id)

        return sels ? sels[0] : null;
    },

    perfis() {
        return perfis;
    },
    perfil(_, { id }) {
        const sels = perfis 
            .filter(p => p.id === id)

        return sels ? sels[0] : null;
    }
}