const usuarios = [
    { 
        id: 1, 
        nome: "Natália Dionizio Naville", 
        email: "natydio@gmail.com", 
        idade: 2, 
        perfil_id: 1, 
        status: 'ATIVO' 
    },
    { 
        id: 2, 
        nome: "Helena Dionizio Naville", 
        email: "helenadio@gmail.com", 
        idade: 36, 
        perfil_id: 2, 
        status: 'INATIVO' 
    },
    { 
        id: 3, 
        nome: "Rafael Naville Contesini", 
        email: "rafael.nc2@gmail.com", 
        idade: 34, 
        perfil_id: 1, 
        status: 'BLOQUEADO' 
    }
];

const perfis = [
    { 
        id: 1, 
        nome: 'Comum'
    },
    { 
        id: 2, 
        nome: 'Administrador'
    }
];

module.exports = { usuarios, perfis };
