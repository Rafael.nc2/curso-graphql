const db = require('../../config/db');
const bcrypt = require('bcrypt-nodejs');
const { getUsuarioLogado } = require('../Comum/usuario');

module.exports = {
    async login(_, { dados }) {
        const usuario = await db('usuarios')
            .where({ email: dados.email })
            .first();

        if(!usuario) {
            throw new Error('Usuario / Senha inválido');
        }

        const saoIguais = bcrypt.compareSync(dados.senha, usuario.senha);
        
        if(!saoIguais) {
            throw new Error('USuário / Senha inválido');
        }

        return getUsuarioLogado(usuario);
    },

    usuarios(parent, args, ctx) {
        // Apenas usuários admin podem consultar todos os usuários
        ctx && ctx.validarAdmin();
        return db('usuarios')
    },

    usuario(_, { filtro }, ctx) {
        // Apenas administrador e o usuário dono dos dados podem consultar 
        ctx && ctx.validarUsuarioFiltro();

        if(!filtro) return null
        const { id, email } = filtro
        if(id) {
            return db('usuarios')
                .where({ id })
                .first()
        } else if(email) {
            return db('usuarios')
                .where({ email })
                .first()
        } else {
            return null
        }
    },
}